﻿namespace VolumeFunctions
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.tbDiameter = new System.Windows.Forms.TextBox();
            this.tbBottomL = new System.Windows.Forms.TextBox();
            this.tbRange = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblVol = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblGal = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbBottomW = new System.Windows.Forms.TextBox();
            this.lblVizPct = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Cube",
            "Vertical Cylinder",
            "Horizontal Cylinder",
            "Stick Chart",
            "Spherical",
            "Retention Pond",
            "Horizontal Oval",
            "Vertical Oval"});
            this.comboBox1.Location = new System.Drawing.Point(279, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(255, 39);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Length (in.)";
            this.label2.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 32);
            this.label3.TabIndex = 1;
            this.label3.Text = "Width (in.)";
            this.label3.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 32);
            this.label4.TabIndex = 1;
            this.label4.Text = "Height (in.)";
            this.label4.Click += new System.EventHandler(this.label1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 301);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 32);
            this.label5.TabIndex = 1;
            this.label5.Text = "Diameter (in.)";
            this.label5.Click += new System.EventHandler(this.label1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 370);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(200, 32);
            this.label6.TabIndex = 1;
            this.label6.Text = "Bottom Length";
            this.label6.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(279, 94);
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(197, 38);
            this.tbLength.TabIndex = 2;
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(279, 163);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(197, 38);
            this.tbWidth.TabIndex = 2;
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(279, 232);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(197, 38);
            this.tbHeight.TabIndex = 2;
            // 
            // tbDiameter
            // 
            this.tbDiameter.Location = new System.Drawing.Point(279, 301);
            this.tbDiameter.Name = "tbDiameter";
            this.tbDiameter.Size = new System.Drawing.Size(197, 38);
            this.tbDiameter.TabIndex = 2;
            // 
            // tbBottomL
            // 
            this.tbBottomL.Location = new System.Drawing.Point(279, 370);
            this.tbBottomL.Name = "tbBottomL";
            this.tbBottomL.Size = new System.Drawing.Size(197, 38);
            this.tbBottomL.TabIndex = 2;
            // 
            // tbRange
            // 
            this.tbRange.Location = new System.Drawing.Point(561, 140);
            this.tbRange.Name = "tbRange";
            this.tbRange.Size = new System.Drawing.Size(197, 38);
            this.tbRange.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(555, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 32);
            this.label7.TabIndex = 1;
            this.label7.Text = "Range (in.)";
            this.label7.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblVol
            // 
            this.lblVol.AutoSize = true;
            this.lblVol.Location = new System.Drawing.Point(555, 263);
            this.lblVol.Name = "lblVol";
            this.lblVol.Size = new System.Drawing.Size(31, 32);
            this.lblVol.TabIndex = 3;
            this.lblVol.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(783, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 68);
            this.button1.TabIndex = 4;
            this.button1.Text = "Calculate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Container Type";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(555, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 32);
            this.label8.TabIndex = 1;
            this.label8.Text = "Cubic Inches";
            this.label8.Click += new System.EventHandler(this.label1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(555, 334);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 32);
            this.label9.TabIndex = 1;
            this.label9.Text = "Gallons";
            this.label9.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblGal
            // 
            this.lblGal.AutoSize = true;
            this.lblGal.Location = new System.Drawing.Point(555, 366);
            this.lblGal.Name = "lblGal";
            this.lblGal.Size = new System.Drawing.Size(31, 32);
            this.lblGal.TabIndex = 3;
            this.lblGal.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 435);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(185, 32);
            this.label10.TabIndex = 1;
            this.label10.Text = "Bottom Width";
            this.label10.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbBottomW
            // 
            this.tbBottomW.Location = new System.Drawing.Point(279, 435);
            this.tbBottomW.Name = "tbBottomW";
            this.tbBottomW.Size = new System.Drawing.Size(197, 38);
            this.tbBottomW.TabIndex = 2;
            // 
            // lblVizPct
            // 
            this.lblVizPct.AutoSize = true;
            this.lblVizPct.Location = new System.Drawing.Point(555, 473);
            this.lblVizPct.Name = "lblVizPct";
            this.lblVizPct.Size = new System.Drawing.Size(31, 32);
            this.lblVizPct.TabIndex = 6;
            this.lblVizPct.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(555, 441);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(142, 32);
            this.label12.TabIndex = 5;
            this.label12.Text = "Visual Pct";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 561);
            this.Controls.Add(this.lblVizPct);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblGal);
            this.Controls.Add(this.lblVol);
            this.Controls.Add(this.tbBottomW);
            this.Controls.Add(this.tbBottomL);
            this.Controls.Add(this.tbDiameter);
            this.Controls.Add(this.tbHeight);
            this.Controls.Add(this.tbWidth);
            this.Controls.Add(this.tbRange);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "KEv2 Volume Fiddler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.TextBox tbDiameter;
        private System.Windows.Forms.TextBox tbBottomL;
        private System.Windows.Forms.TextBox tbRange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblVol;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblGal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbBottomW;
        private System.Windows.Forms.Label lblVizPct;
        private System.Windows.Forms.Label label12;
    }
}

