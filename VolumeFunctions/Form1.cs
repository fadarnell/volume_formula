﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VolumeFunctions
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            tbLength.Text = "0";
            tbWidth.Text = "0";
            tbHeight.Text = "0";
            tbDiameter.Text = "0";
            tbBottomL.Text = "0";
            tbBottomW.Text = "0";
            tbRange.Text = "0";

        }


        private void button1_Click(object sender, EventArgs e)
        {
            double vol = 0;
            int l, bl = 0;
            int w, bw = 0;
            int h = 0;
            int d = 0;
            int range = 0;
            double R, r, theta, A, pct  = 0;

            try
            {
                l = Convert.ToInt32(tbLength.Text);
                w = Convert.ToInt32(tbWidth.Text);
                h = Convert.ToInt32(tbHeight.Text);
                d = Convert.ToInt32(tbDiameter.Text);
                bl = Convert.ToInt32(tbBottomL.Text);
                bw = Convert.ToInt32(tbBottomW.Text);
                range = Convert.ToInt32(tbRange.Text);
            }
            catch (Exception ex) 
            {
                return;
            }

            switch (comboBox1.SelectedItem)
            {
                case "Cube":
                    // volume of a cube (l*w*(h-headspace))
                    vol = l * w * (h - range);

                    // calculate visual pct
                    pct = (double)(h - range) / h;
                    lblVizPct.Text = Math.Round(pct*100, 0).ToString() + "%";

                    break;

                case "Vertical Cylinder":
                    // volume of a Cylinder v = pi * r^2 * (h - headspace)
                    vol = Math.PI * Math.Pow((d / 2.0), 2) * (h - range);

                    // calculate visual pct
                    pct = (double)(h - range) / h;
                    lblVizPct.Text = Math.Round(pct * 100, 0).ToString() + "%";

                    break;

                case "Horizontal Cylinder":
                    // calculate the Radius (R) and distance to chord (r)
                    R = h / 2;
                    r = Math.Abs(R - range);

                    // calculate the angle of the sector (radians)
                    theta = 2 * Math.Acos(r / R);

                    // calculate the area of the segment
                    A = ((theta - Math.Sin(theta)) /2) * Math.Pow(R,2);

                    // if range < Radius (R) then subtract this segments area from the circles area
                    if (range < R)
                        A = (Math.PI * Math.Pow(R, 2)) - A;

                    // compute the volume
                    vol = A * l;

                    // calculate visual pct
                    pct = (double)(h - range) / h;
                    lblVizPct.Text = Math.Round(pct * 100, 0).ToString() + "%";

                    break;

                case "Stick Chart":
                    // IRL this uses the lookup table associated to the tanks standard model
                    // this key-value pair dictionary is for demonstration purposes

                    lblVol.Text = "Stick Chart";

                    // calculate visual pct
                    // for stick charts the h = the maximum distance of travel from empty to full
                    // pct = ((StickChart.Max - StickChart.Min) - range) / (StickChart.Max - StickChart.Min);
                    break;

                case "Spherical":
                    // Vol = (pi * range^2)/3 * ((3 * radius) - range)
                    // calculate the Radius (R)
                    R = d / 2;

                    // calculate the volume
                    vol = (Math.PI * Math.Pow(range, 2) / 3) * ((3 * R) - range);

                    // calculate visual pct
                    pct = (double)(d - range) / d;
                    lblVizPct.Text = Math.Round(pct * 100, 0).ToString() + "%";

                    break;

                case "Retention Pond":
                    // total pond volume = volume of an obelisk

                    // Step 1: Given the top and bottom dimensions, find the angle of the length side
                    // Tan(0) = ((TopW - BottomW) / 2) / Height
                    double thetaL = Math.Atan(((w - bw) / 2) / (double)h);

                    // Step 2: Given the top and bottom dimensions, find the angle of the width side
                    // Tan(0) = ((TopL - BottomL) / 2) / Height
                    double thetaW = Math.Atan(((l - bl) / 2) / (double)h);

                    // Step 3: given the range measurement and the angle of the length side, determine the horizontal displacement of the width side 
                    // Tan(0) = opp / adj
                    double disW = Math.Tan(thetaL) * range;

                    // Step 4: given the range measurement and the angle of the width side, determine the horizontal displacement of the length side
                    // Tan(0) = opp / adj
                    double disL = Math.Tan(thetaW) * range;

                    // Step 5: Calculate the resulting length and width of the surface of the contents
                    // Surface Length = l - (2 * length displacement)
                    // Surface Width = w - (2 * width displacement)
                    double sl = l - (2 * disL);
                    double sw = w - (2 * disW);

                    // Step 6: using the surface dimensions, the bottom dimensions, and the difference between height and range, use the Obelisk formula to compute the resulting volume
                    // https://keisan.casio.com/exec/system/1297652433
                    double oh = h - range;
                    vol = (oh / 6) * ((sl * bw) + (bl * sw) + (2 * ((bl * bw) + (sl * sw))));
                    
                    // calculate visual pct
                    pct = (double)(h - range) / h;
                    lblVizPct.Text = Math.Round(pct * 100, 0).ToString() + "%";
                    
                    break;

                case "Horizontal Oval":
                    lblVol.Text = "Horizontal Oval";
                    break;

                case "Vertical Oval":
                    lblVol.Text = "Vertical Oval";
                    break;

                default:
                    lblVol.Text = "";
                    break;
            }

            lblVol.Text = vol.ToString();
            lblGal.Text = (vol * .004329).ToString();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbLength.Enabled = false;
            tbWidth.Enabled = false;
            tbHeight.Enabled = false;
            tbDiameter.Enabled = false;
            tbBottomL.Enabled = false;
            tbBottomW.Enabled = false;

            switch (comboBox1.SelectedItem)
            {
                case "Cube":
                    tbLength.Enabled = true;
                    tbWidth.Enabled = true;
                    tbHeight.Enabled = true; // this is used in comparison to range to determine graphical pct
                    break;

                case "Vertical Cylinder":
                    tbHeight.Enabled = true; // this is used in comparison to range to determine graphical pct
                    tbDiameter.Enabled = true;
                    break;

                case "Horizontal Cylinder":
                    tbLength.Enabled = true;
                    tbHeight.Enabled = true; // this is used in comparison to range to determine graphical pct
                    break;

                case "Spherical":
                    tbDiameter.Enabled = true; // this is used in comparison to range to determine graphical pct
                    break;

                case "Retention Pond":
                    tbLength.Enabled = true;
                    tbWidth.Enabled = true;
                    tbHeight.Enabled = true; // this is used in comparison to range to determine graphical pct
                    tbBottomL.Enabled = true;
                    tbBottomW.Enabled = true;
                    break;

                default:
                    break;
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}
